import Sidebar from '../../components/Sidebar'
import Navbar from '../../components/Navbar'
import Widget from '../../components/Widget'
import Featured from '../../components/Featured'
import TableList from '../../components/TableList'
import Chart from '../../components/Chart'

import './styles.scss'

export default function Home() {
  return (
    <div className="home">
      <Sidebar />
      <div className="homeContainer">
        <Navbar />
        <div className="widgets">
          <Widget type="user" />
          <Widget type="order" />
          <Widget type="earning" />
          <Widget type="balance" />
        </div>
        <div className="charts">
          <Featured />
          <Chart />
        </div>
        <div className="listContainer">
          <div className="listTitle">Últimas vendas</div>
          <TableList />
        </div>
      </div>
    </div>
  )
}

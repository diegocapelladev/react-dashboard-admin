import Sidebar from '../../components/Sidebar'
import Navbar from '../../components/Navbar'

import './styles.scss'
import DataTable from '../../components/DataTable'

export default function List() {
  return (
    <div className="list">
      <Sidebar />
      <div className="listContainer">
        <Navbar />
        <DataTable />
      </div>
    </div>
  )
}

import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined'
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined'
import PaidOutlinedIcon from '@mui/icons-material/PaidOutlined'
import AccountBalanceWalletOutlinedIcon from '@mui/icons-material/AccountBalanceWalletOutlined'

import './styles.scss'

export default function Widget({ type }) {
  let data

  const amount = 100
  const diff = 10

  switch (type) {
    case 'user':
      data = {
        title: 'Usuários',
        isMoney: false,
        link: 'Ver usuários',
        icon: <PersonOutlineOutlinedIcon className="icon user" />
      }
      break

    case 'order':
      data = {
        title: 'Pedidos',
        isMoney: false,
        link: 'Ver pedidos',
        icon: <ShoppingCartOutlinedIcon className="icon order" />
      }
      break

    case 'earning':
      data = {
        title: 'Ganhos',
        isMoney: true,
        link: 'Ver ganhos',
        icon: <PaidOutlinedIcon className="icon earning" />
      }
      break

    case 'balance':
      data = {
        title: 'Saldo',
        isMoney: true,
        link: 'Ver detalhes',
        icon: <AccountBalanceWalletOutlinedIcon className="icon balance" />
      }
      break
  }

  return (
    <div className="widget">
      <div className="left">
        <span className="title">{data.title}</span>
        <span className="counter">
          {data.isMoney && 'R$'} {amount}
        </span>
        <span className="link">{data.link}</span>
      </div>

      <div className="right">
        <div className="percentage positive">
          <KeyboardArrowUpIcon />
          {`${diff}%`}
        </div>
        {data.icon}
      </div>
    </div>
  )
}

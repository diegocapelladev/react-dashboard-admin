import {
  Paper,
  TableBody,
  TableCell,
  TableContainer,
  TableRow
} from '@mui/material'
import { Table } from '@mui/material'
import { TableHead } from '@mui/material'

import './styles.scss'

const rows = [
  {
    id: 1143155,
    product: 'Acer Nitro 5',
    img: 'https://m.media-amazon.com/images/I/81bc8mA3nKL._AC_UY327_FMwebp_QL65_.jpg',
    customer: 'John Smith',
    date: '01 Maio',
    amount: 1,
    method: 'Boleto Bancário',
    status: 'Aprovado'
  },
  {
    id: 2235235,
    product: 'Playstation 5',
    img: 'https://m.media-amazon.com/images/I/31JaiPXYI8L._AC_UY327_FMwebp_QL65_.jpg',
    customer: 'Michael Doe',
    date: '03 Maio',
    amount: 2,
    method: 'Cartão de Crédito',
    status: 'Aprovado'
  },
  {
    id: 2342353,
    product: 'Redragon S101',
    img: 'https://m.media-amazon.com/images/I/71kr3WAj1FL._AC_UY327_FMwebp_QL65_.jpg',
    customer: 'John Smith',
    date: '04 Maio',
    amount: 5,
    method: 'Boleto Bancário',
    status: 'Pendente'
  },
  {
    id: 2357741,
    product: 'Razer Blade 15',
    img: 'https://m.media-amazon.com/images/I/71wF7YDIQkL._AC_UY327_FMwebp_QL65_.jpg',
    customer: 'Jane Smith',
    date: '04 Maio',
    amount: 2,
    method: 'Cartão de Crédito',
    status: 'Aprovado'
  },
  {
    id: 2342355,
    product: 'ASUS ROG Strix',
    img: 'https://m.media-amazon.com/images/I/81hH5vK-MCL._AC_UY327_FMwebp_QL65_.jpg',
    customer: 'Harold Carol',
    date: '06 Maio',
    amount: 3,
    method: 'Cartão de Crédito',
    status: 'Pendente'
  }
]

export default function TableList() {
  return (
    <TableContainer component={Paper} className="table">
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Código</TableCell>
            <TableCell>Produto</TableCell>
            <TableCell>Cliente</TableCell>
            <TableCell>Data</TableCell>
            <TableCell>Quantidade</TableCell>
            <TableCell>Forma de pagamento</TableCell>
            <TableCell>Status</TableCell>
          </TableRow>
        </TableHead>

        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.id}</TableCell>
              <TableCell>
                <div className="cellWrapper">
                  <img src={row.img} alt="" className="image" />
                  {row.product}
                </div>
              </TableCell>
              <TableCell>{row.customer}</TableCell>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.amount}</TableCell>
              <TableCell>{row.method}</TableCell>
              <TableCell>
                <span className={`status ${row.status}`}>{row.status}</span>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

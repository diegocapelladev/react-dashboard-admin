import MoreVertIcon from '@mui/icons-material/MoreVert'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import { CircularProgressbar } from 'react-circular-progressbar'
import 'react-circular-progressbar/dist/styles.css'

import './styles.scss'

export default function Featured() {
  const percentage = 70
  return (
    <div className="featured">
      <div className="top">
        <h1 className="title">Receita total</h1>
        <MoreVertIcon fontSize="small" />
      </div>
      <div className="bottom">
        <div className="featuredChart">
          <CircularProgressbar
            value={percentage}
            text={`${percentage}%`}
            strokeWidth={5}
          />
        </div>
        <p className="title">Total de vendas hoje</p>
        <p className="amount">R$ 4.200,00</p>
        <p className="desc">
          Processamento de transações anteriores. Últimos pagamentos podem não
          esta incluídos.
        </p>
        <div className="summary">
          <div className="item">
            <div className="itemTitle">Hoje</div>
            <div className="itemResult negative">
              <KeyboardArrowDownIcon fontSize="small" />
              <div className="resultAmount">R$ 12.4k</div>
            </div>
          </div>

          <div className="item">
            <div className="itemTitle">Última semana</div>
            <div className="itemResult positive">
              <KeyboardArrowUpIcon fontSize="small" />
              <div className="resultAmount">R$ 12.4k</div>
            </div>
          </div>

          <div className="item">
            <div className="itemTitle">Último mês</div>
            <div className="itemResult positive">
              <KeyboardArrowUpIcon fontSize="small" />
              <div className="resultAmount">R$ 12.4k</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

import { useContext } from 'react'
import { Link } from 'react-router-dom'
import DashboardIcon from '@mui/icons-material/Dashboard'
import PersonIcon from '@mui/icons-material/Person'
import StoreIcon from '@mui/icons-material/Store'
import CreditCardIcon from '@mui/icons-material/CreditCard'
import LocalShippingIcon from '@mui/icons-material/LocalShipping'
import AssessmentIcon from '@mui/icons-material/Assessment'
import NotificationsIcon from '@mui/icons-material/Notifications'
import SettingsSystemDaydreamIcon from '@mui/icons-material/SettingsSystemDaydream'
import ArticleIcon from '@mui/icons-material/Article'
import SettingsIcon from '@mui/icons-material/Settings'
import AccountCircleIcon from '@mui/icons-material/AccountCircle'
import LogoutIcon from '@mui/icons-material/Logout'

import { DarkModeContext } from '../../context/darkModeContext'
import './styles.scss'

export default function Sidebar() {
  const { dispatch } = useContext(DarkModeContext)

  return (
    <div className="sidebar">
      <div className="top">
        <span className="logo">Logo Admin</span>
      </div>
      <hr />
      <div className="center">
        <ul>
          <p className="title">Principal</p>
          <li>
            <DashboardIcon className="icon" />
            <Link to="/">
              <span>Dashboard</span>
            </Link>
          </li>
          <p className="title">Listas</p>
          <Link to="/users">
            <li>
              <PersonIcon className="icon" />
              <span>Clientes</span>
            </li>
          </Link>
          <Link to="/products">
            <li>
              <StoreIcon className="icon" />
              <span>Produtos</span>
            </li>
          </Link>
          <li>
            <CreditCardIcon className="icon" />
            <span>Pedidos</span>
          </li>
          <li>
            <LocalShippingIcon className="icon" />
            <span>Entregas</span>
          </li>

          <li>
            <AssessmentIcon className="icon" />
            <span>Relatórios</span>
          </li>
          <li>
            <NotificationsIcon className="icon" />
            <span>Notificações</span>
          </li>
          <p className="title">Serviços</p>
          <li>
            <SettingsSystemDaydreamIcon className="icon" />
            <span>Sistema</span>
          </li>
          <li>
            <ArticleIcon className="icon" />
            <span>Logs</span>
          </li>
          <li>
            <SettingsIcon className="icon" />
            <span>Configurações</span>
          </li>
          <p className="title">Usuário</p>
          <li>
            <AccountCircleIcon className="icon" />
            <span>Perfil</span>
          </li>
          <li>
            <LogoutIcon className="icon" />
            <span>Sair</span>
          </li>
        </ul>
      </div>
      <div className="bottom">
        <p className="title">Temas</p>
        <div className="bottom-content">
          <div
            className="colorOption"
            onClick={() => dispatch({ type: 'LIGHT' })}
          ></div>
          <div
            className="colorOption"
            onClick={() => dispatch({ type: 'DARK' })}
          ></div>
        </div>
      </div>
    </div>
  )
}

import { useContext } from 'react'
import SearchIcon from '@mui/icons-material/Search'
import LanguageIcon from '@mui/icons-material/Language'
import NightlightOutlinedIcon from '@mui/icons-material/NightlightOutlined'
import FullscreenExitOutlinedIcon from '@mui/icons-material/FullscreenExitOutlined'
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined'
import ChatOutlinedIcon from '@mui/icons-material/ChatOutlined'
import ListOutlinedIcon from '@mui/icons-material/ListOutlined'

import { DarkModeContext } from '../../context/darkModeContext'
import './styles.scss'

export default function Navbar() {
  const { dispatch } = useContext(DarkModeContext)
  return (
    <div className="navbar">
      <div className="wrapper">
        <div className="search">
          <input type="text" placeholder="Buscar..." />
          <SearchIcon className="searchIcon" />
        </div>
        <div className="items">
          <div className="item">
            <LanguageIcon className="icon" />
            <span>pt-BR</span>
          </div>
          <div className="item">
            <NightlightOutlinedIcon
              className="icon"
              onClick={() => dispatch({ type: 'TOGGLE' })}
            />
          </div>
          <div className="item">
            <FullscreenExitOutlinedIcon className="icon" />
          </div>
          <div className="item">
            <NotificationsNoneOutlinedIcon className="icon" />
            <div className="counter">1</div>
          </div>
          <div className="item">
            <ChatOutlinedIcon className="icon" />
            <div className="counter">3</div>
          </div>
          <div className="item">
            <ListOutlinedIcon className="icon" />
          </div>
          <div className="item">
            <img
              src="https://images.pexels.com/photos/762020/pexels-photo-762020.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
              alt="Perfil"
              className="avatar"
            />
          </div>
        </div>
      </div>
    </div>
  )
}
